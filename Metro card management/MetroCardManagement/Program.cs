﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroCardManagement
{
    class Program 
    {
        public static List<UserAccount> userAccountList = new List<UserAccount>();

        public static List<TravelHistory> travelHistoryList = new List<TravelHistory>();
        static void Main(string[] args)
        {
            //creating new user

            UserAccount newUser1= new UserAccount("dhanush",9025538754,100);
            UserAccount newUser2 = new UserAccount("ajith", 8220358565, 200);
            UserAccount newUser3= new UserAccount("yashvanth",8940372967, 300);

            //Adding to List
            userAccountList.Add(newUser1);
            userAccountList.Add(newUser2);
            userAccountList.Add(newUser3);

            int choice1;
            string Userinput = string.Empty;
            string userfunctionPick = option.YES.ToString();
            //1.New user 
            do
            {
                Console.WriteLine("---------------------------------");
                Console.WriteLine("Welcome to Metro Station");
                Console.WriteLine("---------------------------------");
                Console.WriteLine("1.New user \n2.Existing user");
                Console.WriteLine("Enter your choice");
                choice1 = int.Parse(Console.ReadLine());
                switch (choice1)
                {
                    case 1:
                        //Creating user1
                        CreateUserAccount();
                        userfunctionPick = option.NO.ToString();
                        break;

                    case 2:
                        //2.Existing user
                        string cardNumber;
                        string existingUserPick = option.YES.ToString();
                        do
                        { 
                            Console.WriteLine("enter your card number");
                            cardNumber = Console.ReadLine();
                            var userObject = userAccountList.Find(user => user.cardNumber.Equals(cardNumber));
                            if (userAccountList.Contains(userObject))
                            {
                                Console.WriteLine($"name :{userObject.userName}\nphone :{userObject.userPhoneNumber}\nbalance :{userObject.balance}");
                                existingUserPick = option.NO.ToString();
                            }
                                
                            else
                            {
                                Console.WriteLine("invalid card number");
                                existingUserPick = option.YES.ToString();
                            }

                        } while (existingUserPick ==option.YES.ToString());
                        existingUserPick = option.YES.ToString();
                        do
                        {
                            //Existing user options
                            Console.WriteLine("---------------------------------");
                            Console.WriteLine("Enter menu to continue");
                            Console.WriteLine("---------------------------------");
                            Console.WriteLine("1.Balance\n2.Recharge\n3.travel\n4.Travel history");
                            Console.WriteLine("Enter your choice:");
                            int choice;
                            choice = int.Parse(Console.ReadLine());
                            var userObject = userAccountList.Find(user => user.cardNumber.Equals(cardNumber));
                            switch (choice)
                            {
                                //1.Balance option

                                case 1:
                                    if (userAccountList.Contains(userObject))
                                    {
                                        Console.WriteLine($"UserName :{userObject.userName} | balance :{userObject.balance}");

                                    }
                                    existingUserPick = option.NO.ToString();
                                    break;



                                //2.Recharge option
                                case 2:
                                    if (userAccountList.Contains(userObject))
                                    {
                                        decimal amount;
                                        Console.WriteLine("Enter your amount to recharge");
                                        amount = decimal.Parse(Console.ReadLine());
                                        userObject.balance += amount;
                                        Console.WriteLine("Recharge successfull");
                                        Console.WriteLine($"your updated balance is :{userObject.balance}");
                                    }
                                    existingUserPick = option.NO.ToString();
                                    break;


                                //Travel option 
                                case 3:
                                    int secondChoice;
                                    Console.WriteLine("---------------------------------");
                                    Console.WriteLine("Choose your station to travel :");
                                    Console.WriteLine("---------------------------------");

                                    Console.WriteLine("1.Anna nagar Fare(RS.20)\n2.kilpauk  Fare(RS.40)");
                                    secondChoice = int.Parse(Console.ReadLine());

                                    int annaNagarfare = 20;
                                    int kilpaukfare = 40;
                                    decimal userAccountBalance = userObject.balance;
                                    switch (secondChoice)
                                    {

                                        case 1:
                                            if (userAccountBalance - annaNagarfare < 0)
                                            {
                                                Console.WriteLine("Insufficient balance");
                                            }
                                            else
                                            {
                                                userObject.balance -= annaNagarfare;
                                                var historyObject = new TravelHistory(cardNumber, travelStation.AnnaNagar.ToString(), annaNagarfare, DateTime.Now);
                                                Console.WriteLine($"Anna nagar fare {annaNagarfare} is deducted");
                                                Console.WriteLine($"your updated balance is {userObject.balance}");
                                                travelHistoryList.Add(historyObject);
                                                Console.WriteLine("Travel completed");
                                            }
                                            break;
                                        case 2:
                                            if (userAccountBalance - kilpaukfare < 0)
                                            {
                                                Console.WriteLine("Insufficient balance");
                                            }
                                            else
                                            {
                                                userObject.balance -= kilpaukfare;
                                                var historyObject = new TravelHistory(cardNumber, travelStation.kilpauk.ToString(), kilpaukfare, DateTime.Now);
                                                Console.WriteLine($"kilpauk fare is{kilpaukfare} is deducted");
                                                Console.WriteLine($"your updated balance is {userObject.balance}");
                                                travelHistoryList.Add(historyObject);
                                                Console.WriteLine("Travel completed");
                                            }


                                            break;

                                        default:
                                            Console.WriteLine("invalid choice");
                                            break;

                                    }
                                    existingUserPick = option.NO.ToString();
                                    break;
                                //TravelHistory
                                case 4:

                                    foreach (var travelHistory in travelHistoryList)
                                    {
                                        if (travelHistory.cardNumber.Equals(travelHistory.cardNumber))
                                        {

                                            Console.WriteLine($"travel Id :{travelHistory.TravelId} | cardNumber{travelHistory.cardNumber} | Travel Station :{travelHistory.travelStation} | Fare :{travelHistory.fare} | Travel Time{travelHistory.travelTime}");

                                        }

                                    }
                                    existingUserPick = option.NO.ToString();
                                    break;
                                default:
                                    Console.WriteLine("Invalid choice");
                                    existingUserPick = option.NO.ToString();
                                    break;

                            }

                         Console.WriteLine("do you want to continue yes or no");
                         existingUserPick = Console.ReadLine().ToUpper();

                        } while (existingUserPick == option.YES.ToString());
                        existingUserPick = option.YES.ToString();
                        break;
                    default:
                        Console.WriteLine("invalid choice");
                        break;

                }
                Console.WriteLine("do you want to continue yes or no");
                userfunctionPick=Console.ReadLine().ToUpper();

            } while (userfunctionPick == option.YES.ToString());


            void CreateUserAccount()
            {
                string userName = string.Empty;
                long phoneNumber;
                decimal initialBalance = 0;
                Console.WriteLine("Enter your name :");
                userName = Console.ReadLine();
                Console.WriteLine("Enter your phonenumber :");
                phoneNumber = long.Parse(Console.ReadLine());
                Console.WriteLine("Enter your initialbalance :");
                initialBalance = decimal.Parse(Console.ReadLine());
                UserAccount user1 = new UserAccount(userName, phoneNumber, initialBalance);
                userAccountList.Add(user1);
                Console.WriteLine("Account is created successfully");
            }

        }
       






















        
    }
}
 public enum travelStation
{
    AnnaNagar,
    kilpauk
}
public enum option
{
    YES,
    NO
}
