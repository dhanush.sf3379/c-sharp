﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroCardManagement
{
    class TravelHistory
    {
        public string TravelId { get; }

        private static int travelIdSeed = 500; 

        public string cardNumber { get; }

        public string travelStation { get; set; }

        public int fare { get; }

        public DateTime travelTime { get; set; }

        public TravelHistory(string cardNumber,string travelStation,int fare,DateTime travelTime)
        {
            this.cardNumber = cardNumber;
            this.travelStation = travelStation;
            this.fare = fare;
            this.travelTime = travelTime;
            this.TravelId = travelIdSeed.ToString();
            travelIdSeed++;
        }
    }
}
