﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroCardManagement
{
    class UserAccount
    {
        public string cardNumber{ get; }

        private static int cardNumberSeed = 1000;

        public string userName  { get; set; }

        public long userPhoneNumber { get; set; }

        public decimal balance { get; set; }

        public UserAccount(string userName,long phoneNumber,decimal initialBalance)
        {
            this.userName = userName;
            this.userPhoneNumber = phoneNumber;
            this.balance = initialBalance;
            this.cardNumber = cardNumberSeed.ToString();
            cardNumberSeed++;

        }
        
    }
}
